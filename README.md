# Theme #001

Kitchen retailer template set up with Yeoman ZF5 generator - https://github.com/juliancwirko/generator-zf5

## Usage

Clone this repo to your local machine or fork it to another repo before use. Run `bower install` and `npm install` (might need `sudo` prefixed) to get your dependencies. You are now ready to rock!


### Templates
* Frontpage
* Standard subpage
* Product category
* Product list
* Product detail
* References list
* References detail
* News list
* News detail
* Contactpage
* Searchresults

All pages use #siteHeader and #siteFooter, these are defined in header and footer partials. NOTE: The sticky footer styles has its own partial.

Template specific info:

#### Frontpage

The frontpage has a static communicationfield on top followed by optional blocks. Every optional block is standalone and is defined in its own DNA template. Include block in the _frontpageLayout template for use.

#### Subpages

General info: Every subpage has the same structure, this is defined in DNA _standardpage. This is where the page filtration happens. These pages all have the #pageHeader which includes the page name and breadcrumb. The #pageContent block is used for page specific content.