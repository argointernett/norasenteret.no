/* global google */


var app = (function(document, $) {

	'use strict';
	var docElem = document.documentElement,

		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},



		_initGoogleMaps = function(){

			var mapCanvas = document.getElementById('map-canvas');

			// Find latlng in data attribute and parse it
			var mapPosition = $(mapCanvas).data('position');
			mapPosition = mapPosition.split(',');
			var pos1 = parseFloat(mapPosition[0]);
			var pos2 = parseFloat(mapPosition[1]);

			var latLng = new google.maps.LatLng(pos1,pos2);

			var mapOptions = {
				center: latLng,
				zoom: 11,
				mapTypeId: google.maps.MapTypeId.ROADMAP
	    	};
		    var	map = new google.maps.Map(mapCanvas, mapOptions);

		    var marker = new google.maps.Marker({
		          position: latLng,
		          map: map,
		          title: 'Hello World!'
		    });

		},



		_swipeBox = function(){

			if($('#swipebox').length > 0 || $('#galleryContainer').length > 0) {
				$('a', '#galleryContainer').swipebox();
				$('a', '#swipebox').swipebox();
			} else {
				console.log('no swipebox for you!');
				return false;
			}
			

		},

		// Checks input for value length and gives focus to input if value is less then 2
		_topbarSearch = function(){

			var $input = $('input', '.topBar'),
				$form = $('form', '.topBar');

			$form.on('submit', function(event){
				if($input.val().length < 2) {
					event.preventDefault();
					$input.focus();
				} else {
					console.log('send searchString');
				}


			});


		},


		_siteSearch = function(event){

			event.preventDefault();
			event.stopPropagation();

			var form = $('#siteSearchForm'),
				open = form.hasClass('open'),
				button = $('a.siteSearch'),
				input = $('input[name="searchString"]');

			input.on('click', function(e){
				e.stopPropagation();
			});

			if (open) {

				form.removeClass('open');
				button.removeClass('active');
				$('body').off('click', _siteSearch);

			} else {

				form.addClass('open');
				button.addClass('active');
				form.find('input').focus();
				$('body').on('click', _siteSearch);

			}

		},



		_registerEvents = function(){

			$('#menuToggle').on('click', function(){

				$('#siteNavigation').stop().slideToggle(500);

			});

			$('.siteSearch').on('click', _siteSearch);

			$('#sidebarToggle').on('click', function(){

				var $this = $(this);
				$this.toggleClass('open');
				var open = $this.hasClass('open');

				if(open){
					$this.html('Lukk undermeny');
				} else {
					$this.html('Vis undermeny');
				}

				$('.side-nav').stop().slideToggle(400);

			});

		},


		_socialSharingCounts = function(){

			var location = window.location.href;
			var getCountUrl = '';

			var _setShareCount = function(url, button) {
				var container = $('.sharecount', button);
				$.ajax({
					url: url,
					dataType: 'jsonp',
					success: function(response){
						container.html(response.shares || response.count);
					}
				});
			};

			// Setter sharecount
			$('.share', '.sharing').each(function(){
				var $button = $(this);
				var shareTo = $button.data('shareto');
				switch(shareTo) {
					case 'facebook':
						getCountUrl = 'http://graph.facebook.com/?id=' + location;
						_setShareCount(getCountUrl, $button);
						break;
					case 'twitter':
						getCountUrl = 'http://urls.api.twitter.com/1/urls/count.json?url=' + location;
						_setShareCount(getCountUrl, $button);
						break;
					case 'linkedin':
						getCountUrl = 'http://www.linkedin.com/countserv/count/share?url=' + location + '&format=json';
						_setShareCount(getCountUrl, $button);
						break;
					default:
						alert('Kunne ikke hente delingsantall');

				}
			});



			// Delingsfunksjoner
			// Disabled facebook appId 
			$('a', '.sharing').on('click', function(){

				var shareTo = $(this).data('shareto');
				// var fbAppId = 768043129929149;

				var width, height, left, top, opts, shareString;

				switch(shareTo) {
					case 'facebook':

						width 	= 520;
						height 	= 570;
						left 	= ($(window).width() - width) / 2;
						top 	= ($(window).height() - height) / 2;
						opts 	= 'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;


						shareString = 'https://www.facebook.com/dialog/share?';
						// shareString += 'app_id='+fbAppId;
						shareString += '&display=popup';
						shareString += '&href='+document.URL;
						shareString += '&redirect_uri='+document.URL;
						window.open(shareString, 'facebook', opts);

						break;

					case 'linkedin':

						width 	= 520;
						height 	= 570;
						left 	= ($(window).width() - width) / 2;
						top 	= ($(window).height() - height) / 2;
						opts 	= 'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;

						shareString = 'http://www.linkedin.com/shareArticle?mini=true';
						shareString += '&url='+document.URL;
						shareString += '&source=Ildne AS';
						window.open(shareString, 'linkedin', opts);

						break;

					case 'twitter':

						width 	= 575;
						height 	= 400;
						left 	= ($(window).width() - width) / 2;
						top 	= ($(window).height() - height) / 2;
						opts 	= 'width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;

						shareString = 'http://twitter.com/share';
						window.open(shareString, 'twitter', opts);

						break;

					default:
						alert('Kunne ikke dele denne siden. Vennligst prøv igjen senere.');
				}

			});

		},



		_init = function() {
			_userAgentInit();
			_swipeBox();
			_registerEvents();
			_topbarSearch();

			if($('.sharing').length > 0) {
				_socialSharingCounts();
			}

			// Checks for slideshow container
			if( $('#slideshow').length > 0 && !$('#slideshow').hasClass('static') ) {
				$('#slideshow').argoslide({
					navIconPrev: 'icon ion-ios-arrow-back',
					navIconNext: 'icon ion-ios-arrow-forward'
				});
			}

		};

	return {
		init: _init,
		gmaps: _initGoogleMaps
	};

})(document, jQuery);



(function() {

	'use strict';
	app.init();

	// Checks that the script is available and map-canvas is existing
	if(typeof google !== 'undefined' && $('#map-canvas').length > 0){
		google.maps.event.addDomListener(window, 'load', app.gmaps());
	}
})();


