$.fn.argoslide = function(options) {

	'use strict';

	var defaults = {
		images: $('.slide', this),
		buttons: $('.slidebtn'),
		navIconPrev: 'icon ion-chevron-left',
		navIconNext: 'icon ion-chevron-right',
		addnavigation: true,
		slidespeed: 6000,
		simple: false
	};
	
	options = $.extend( {}, defaults, options );
	
	return this.each(function(){
	
		var that = this;	
		var action;
		var nr = 0;

		var _prepImages = function(){

			$.each(options.images, function(){

				var image = $(this).children('img').first();
				var imgSrc = image.attr('src');
				$(this).css({'background-image': 'url(' + imgSrc + ')'});

			});

		};


		// Takes a number, removes all active classes, sets the image equal to the number as active	
		var _newImage = function(number) {
			
			options.images.removeClass('active');
			options.images.eq(number).addClass('active');
			options.navitems.removeClass('active');
			options.navitems.eq(number).addClass('active');
			if(!options.simple){
				options.buttons.removeClass('active');
				options.buttons.eq(number).addClass('active');
			}
			_setSlideshowHeight();
		};
		
		var _nextImage = function(){
			if(nr<options.images.length-1){
				nr++;
			} else {
				nr = 0;
			}
			_newImage(nr);
		};
		var _prevImage = function(){
			if(nr === 0){
				nr = options.images.length-1;
			} else {
				nr--;
			}
			_newImage(nr);
		};
		
		// Builds navigation and appends this to the container
		var _addNavigation = function(){

				// Prev next arrow navigation
				var navigation = '<div class="slidenav prev"><i class="' + options.navIconPrev + '"></i></div>';
				navigation += '<div class="slidenav next"><i class="' + options.navIconNext + '"></i></div>';
				$(that).append(navigation);
				
				$('.slidenav').on('click', function(){
					clearInterval(action);
					action = null;
					if ($(this).hasClass('prev')) {
						_prevImage();	
					} else {
						_nextImage();
					}
					_slideshow();
				});


				// Button navigation
				var btnNavigation = '<div class="navcontainer">';
				for(var i=0;i<options.images.length;i++){
					btnNavigation += '<div class="navitem"></div>';
				}
				btnNavigation += '</div>';
				$(that).append(btnNavigation);
				options.navitems = $('.navitem');
				
				$('.navitem').on(Modernizr.touch ? 'touchend' : 'click', function(){
					nr = $('.navitem').index($(this));
					clearInterval(action);
					action = null;
					_newImage(nr);
					_slideshow();
					
				});
				
		};
		

		// initiate the slideshow interval
		var _slideshow = function(){
			action = setInterval(_nextImage, options.slidespeed);
		};

		//
		// Setter størrelse på container og plasserer navigasjon.
		// Init bool lagt inn for å sjekke om funksjonen kjøres ved lasting av side eller ved skalering av nettleservindu.
		// Kjører load event på bildet dersom det er ved lasting
		//
		var _setSlideshowHeight = function(init){


			if(init){

				var src = $(options.images[0]).children('img').first().attr('src');
				var img = new Image();
				img.src = src;
				// $(img).load(function(){
				// 	$('.slideshow').css({'height': $('.item.active', '.slideshow').height()+50});
				// 	$('.navcontainer').css({'top': $('img', '.item.active').height()-40});
				// 	$('.slidenav').css({'top': ($('img', '.item.active').height()/2)-30});
				// });

			} else {
				// $('.slideshow').css({'height': $('.item.active', '.slideshow').height()+50});
				// $('.navcontainer').css({'top': $('img', '.item.active').height()-40});
				// $('.slidenav').css({'top': ($('img', '.item.active').height()/2)-30});
			}
		};
		
		
		
		var init = function(){
			_prepImages();
			options.images.eq(nr).addClass('active');

			if(options.addnavigation) {
				_addNavigation();
			}
			if(!options.simple){
				options.navitems.eq(nr).addClass('active');
			}
			_slideshow();
			_setSlideshowHeight(true);

		};

		// Checks for length of images, kill plugin if no images.
		// Could probably need some refactoring, dont know if  remove is the way to go
		if(options.images.length>0){
			init();
		} else {
			that.remove();
		}

		$(window).on('resize', function(){
			_setSlideshowHeight(false);
		});
		
		// Adds clickfunction to buttons in not simple mode
		if(!options.simple){
			options.buttons.on('click', function(){
				if(!$(this).hasClass('active')){
					nr = options.buttons.index(this);
					_newImage(nr);
				}
			});
		}
	
	});
	
};